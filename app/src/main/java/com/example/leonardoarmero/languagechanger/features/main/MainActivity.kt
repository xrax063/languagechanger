package com.example.leonardoarmero.languagechanger.features.main

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import com.example.leonardoarmero.languagechanger.R
import com.example.leonardoarmero.languagechanger.features.base.BaseActivity
import com.example.leonardoarmero.languagechanger.features.main.fragments.DashboardFragment
import com.example.leonardoarmero.languagechanger.features.main.fragments.HomeFragment
import com.example.leonardoarmero.languagechanger.features.main.fragments.NotificationsFragment
import com.example.leonardoarmero.languagechanger.features.utils.LocaleUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bnNavigator.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        btEnglish.setOnClickListener { changeLanguage(LocaleUtils.EN_LANGUAGE) }
        btSpanish.setOnClickListener { changeLanguage(LocaleUtils.ES_LANGUAGE) }

        supportFragmentManager.beginTransaction()
                .add(R.id.flContainer, HomeFragment.newInstance(), HomeFragment.TAG)
                .commit()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val tag: String
        val fragment = when (item.itemId) {
            R.id.navigation_home -> {
                tag = HomeFragment.TAG
                if (supportFragmentManager.findFragmentByTag(tag) != null) {
                    supportFragmentManager.findFragmentByTag(tag)
                } else HomeFragment.newInstance()
            }
            R.id.navigation_dashboard -> {
                tag = DashboardFragment.TAG
                if (supportFragmentManager.findFragmentByTag(tag) != null) {
                    supportFragmentManager.findFragmentByTag(tag)
                } else DashboardFragment.newInstance()
            }
            R.id.navigation_notifications -> {
                tag = NotificationsFragment.TAG
                if (supportFragmentManager.findFragmentByTag(tag) != null) {
                    supportFragmentManager.findFragmentByTag(tag)
                } else NotificationsFragment.newInstance()
            }
            else -> return@OnNavigationItemSelectedListener false
        }

        supportFragmentManager.beginTransaction()
                .replace(R.id.flContainer, fragment, tag)
                .commit()

        true
    }
}
