package com.example.leonardoarmero.languagechanger.features.login

import android.content.Intent
import android.os.Bundle
import com.example.leonardoarmero.languagechanger.R
import com.example.leonardoarmero.languagechanger.features.base.BaseActivity
import com.example.leonardoarmero.languagechanger.features.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btLogin.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}