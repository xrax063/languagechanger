package com.example.leonardoarmero.languagechanger.features.base

import android.content.Context
import android.support.v4.app.NavUtils
import android.support.v7.app.AppCompatActivity
import com.example.leonardoarmero.languagechanger.features.utils.LocaleUtils


open class BaseActivity : AppCompatActivity() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleUtils.onAttach(base))
    }

    protected fun changeLanguage(languageCode: String) {
        LocaleUtils.setLocale(this, languageCode)
        recreate()
    }

    override fun onBackPressed() {
        NavUtils.navigateUpFromSameTask(this)
    }
}