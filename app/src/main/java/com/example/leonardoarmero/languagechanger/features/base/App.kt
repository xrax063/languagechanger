package com.example.leonardoarmero.languagechanger.features.base

import android.app.Application
import android.content.Context
import com.example.leonardoarmero.languagechanger.features.utils.LocaleUtils

class App: Application() {
    override fun attachBaseContext(base: Context?) {
        if (base == null) super.attachBaseContext(base)
        else super.attachBaseContext(LocaleUtils.onAttach(base, LocaleUtils.EN_LANGUAGE))
    }
}